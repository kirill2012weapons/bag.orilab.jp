<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

wp_enqueue_script( 'credit-card-js',  WCPAYJP_Url . 'include/assets/js/jquery.payment.js', array('jquery'), WCPAYJP_Version , true ); 
wp_enqueue_script( 'tooltip-js',  WCPAYJP_Url . 'include/assets/js/jquery.tooltipster.min.js', array('jquery'), WCPAYJP_Version , true ); 
wp_enqueue_style( 'payjp-style',  WCPAYJP_Url . 'include/assets/css/payjp_style.css' );
?>

<script type="text/javascript">
jQuery(function($) {
	
	$(document).ready(function() {
		$('.tooltip_perso').tooltipster({
			content: $('<img src="<?php echo WCPAYJP_Url . 'include/assets/img/cvc_image.png';  ?>" />'),
			maxWidth: 200,
			theme: 'tooltip-theme-cvc',
			delay: 100
		});
    });

	function valid_card_exp(expMonth, expYear) {
		var now = new Date(); 
		if(!expMonth) expMonth = '13';
		if(!expYear) expYear = '1990';
		var thismonth = (now.getMonth() + 1); 
		var thisyear = now.getFullYear() ; 
		var expireYear = parseInt(expYear); 
		var yearLength = expYear.length; 
		var expireMonth = parseInt(expMonth); 
		if(yearLength == 2){ expireYear += 2000; }
		if ((expireMonth < thismonth && expireYear == thisyear) || expireMonth > 12 || expireYear < thisyear){ 
			$('.cc-exp-month').toggleInputError(true); 
			$('.cc-exp-year').toggleInputError(true); 
		} else {
			$('.cc-exp-month').toggleInputError(false); 
			$('.cc-exp-year').toggleInputError(false); 
		}
	}
		
	function valid_card_name() {
		var re = /^[A-Za-z ]+$/;
		var out = $('.cc-name').val().match(re) ? false : true;
		$('.cc-name').toggleInputError(out);
	}
	
	$.fn.toggleInputError = function(erred) {
		this.closest('.form-row').toggleClass('has-error', erred);
		return this;
	};
	
	$('.cc-number').payment('formatCardNumber');
	$('.cc-exp').payment('formatCardExpiry');
	$('.cc-cvc').payment('formatCardCVC');
  
	$('.cc-number').bind("blur",function(e) {
		var cardType = $.payment.cardType($('.cc-number').val());
		if($('.cc-number').val()) $('.cc-number').toggleInputError(!$.payment.validateCardNumber($('.cc-number').val()));
	});
	
	$('.cc-cvc').bind("blur",function(e) {
		var cardType = $.payment.cardType($('.cc-number').val());
		if($('.cc-cvc').val()) $('.cc-cvc').toggleInputError(!$.payment.validateCardCVC($('.cc-cvc').val(), cardType));
	});
	
	$('.cc-exp-month').bind("change",function(e) {
		console.log( $('.cc-exp-month').val() );
		if($('.cc-exp-year').val()) valid_card_exp($('.cc-exp-month').val(), $('.cc-exp-year').val());
	});
	
	$('.cc-exp-year').bind("change",function(e) {
		console.log( $('.cc-exp-year').val() );
		valid_card_exp($('.cc-exp-month').val(), $('.cc-exp-year').val());
	});
	
	$('.cc-name').bind("blur",function(e) {
		if($('.cc-name').val()) valid_card_name();
	});
	
	$( 'input[name="card_status"]').on("click", function() {
		$('input[name="card_status"]:not(:checked)').siblings('div').slideToggle();
        $('input[name="card_status"]:checked').siblings('div').slideToggle();
	}); 

});
</script>

<?php
/*
delete_user_meta(get_current_user_id(), 'payjp_customer_id');
delete_user_meta(get_current_user_id(), 'payjp_customer_card_list');
*/

$card_list = array();
$user_customer_id = $user_customer_card_list = '';

if ( is_user_logged_in() ) :
	$user_customer_id        = get_user_meta( get_current_user_id(), 'payjp_customer_id', true );
	
	$user_customer_card_list = get_user_meta( get_current_user_id(), 'payjp_customer_card_list', true );
	
	if ( $user_customer_card_list ) :
		
		foreach( $user_customer_card_list as $fingerprint => $card_id ) :
		
			$card_info = $this->retrieve_card( $user_customer_id, $card_id );
			
			if ( empty( $card_info ) ) {
				
				
				
			} else {
				
				$card_list[$fingerprint] = array(
					'type'         => $card_info->brand,
					'last4_card'   => "••••••••••••" . $card_info->last4,
					'card_exp'     => $card_info->exp_month . "/" . $card_info->exp_year,
					'created_date' => date('m/d/y', $card_info->created),
				);
				
			}
			
		endforeach;
		
	endif;
endif;
?>

<div class="woocommerce-payjp-card-form">
	
	<?php if ( !empty($card_list) ): ?>
	
		<div>
		
			<input type="radio" class="" id="card_status_saved" name="card_status" value="saved" <?php echo ( !empty($card_list) ) ? 'checked' : ''; ?>><label for="card_status_saved"><?php _e('登録済のカードを使用する', WCPAYJP_Name); ?></label>
			
			<div class="card-form-content <?php echo ( !empty($card_list) ) ? 'show' : ''; ?>" id="saved_card">
				
				<?php if ( $card_list ) : ?>
					
					<select class="" id="saved_card_key" name="saved_card_key">
						
						<option disabled="disabled" selected="selected"><?php _e('カードを選択', WCPAYJP_Name); ?></option>
						
						<?php foreach( $card_list as $key => $item ) : ?>
					
							<option value=<?php echo $key; ?>><?php echo $item['type'] . ' : ' . $item['last4_card']; ?></option>
							
						<?php endforeach; ?>
					
					</select>
					
				<?php else: ?>
				
					<?php _e('登録されているカードはございません。', WCPAYJP_Name ); ?>
				
				<?php endif; ?>
			</div>
		
		</div>
	
	<?php endif; ?>
	
	<div>
		
		<input type="radio" class="" id="card_status_new" name="card_status" value="<?php echo ( $user_customer_id ) ? 'add' : 'new'; ?>" <?php echo ( empty($card_list) ) ? 'checked' : ''; ?>><label for="card_status_new"><?php echo ( $user_customer_id ) ? __('新しいカードを追加して使用する', WCPAYJP_Name) : __('新しいカードを使用する', WCPAYJP_Name); ?></label>
		
		<div class="card-form-content <?php echo ( empty($card_list) ) ? 'show' : ''; ?>" id="new_card">
			
			<div class="form-row">
			
				<label><span style="font-weight: bold;"><?php _e('Number', '')?> </span> <span class="required">*</span>
					<span class="WP_supported_brands" title="Visa" data-card-type="visa"></span>
					<span class="WP_supported_brands" title="MasterCard" data-card-type="mastercard"></span>
					<span class="WP_supported_brands" title="American Express" data-card-type="american_express"></span>
					<span class="WP_supported_brands" title="Diners Club" data-card-type="diners_club"></span>
					<span class="WP_supported_brands" title="JCB" data-card-type="jcb"></span>
				</label>
				
				<input class="input-text form-control cc-number" id="card_number" name="card_number" maxlength="19" type="tel" pattern="[0-9\s]*" autocomplete="cc-number" placeholder="1234 5678 0912 3456" style="display:block;"/>
				
			</div>
		
			<div class="form-row form-row-exp clearfix">
				
				<label><span style="font-weight: bold;"><?php _e('Expiration Date', '')?></span> <span class="required">*</span></label>
				
				<div>
					<select class="form-control cc-exp-month" id="card_exp_month" name="card_exp_month">
						<option disabled="disabled" selected="selected"><?php _e('Month', '')?></option>
						<option value=1>01</option>
						<option value=2>02</option>
						<option value=3>03</option>
						<option value=4>04</option>
						<option value=5>05</option>
						<option value=6>06</option>
						<option value=7>07</option>
						<option value=8>08</option>
						<option value=9>09</option>
						<option value=10>10</option>
						<option value=11>11</option>
						<option value=12>12</option>
					</select>
				</div>
			
				<div class="date_sep">/</div>
			
				<div>
					<select class="form-control cc-exp-year form-control" id="card_exp_year" name="card_exp_year">
						<option disabled="disabled" selected="selected"><?php _e('Year', '')?></option>
						<?php
						$today = (int)date('Y', time());
						for($i = 0; $i < 10; $i++)
						{
						?>
							<option value="<?php echo $today; ?>"><?php echo $today; ?></option>
						<?php
						$today++;
						}
						?>
					</select>
				</div>
				
			</div>
		
			<div class="form-row">
			
				<label><span style="font-weight: bold;"><?php _e('Cardholder\'s name', '')?></span> <span class="required">*</span></label>
				<input class="input-text form-control cc-name" type="text" id="card_name" name="card_name" placeholder="TARO YAMADA" style="text-transform:uppercase;"/>
				
			</div>
		
			<div class="form-row">
			
				<label><span style="font-weight: bold;"><?php _e('Security Code', '')?></span> <span class="required">*</span> <span id="cvc_help_sign" class="tooltip_perso"></span></label>
				<input class="input-text form-control cc-cvc" type="text" maxlength="4" id="card_cvc" pattern="\d*" autocomplete="off" name="card_cvc" placeholder="•••"/>
				
			</div>
			
		</div>
	
	</div>
</div>