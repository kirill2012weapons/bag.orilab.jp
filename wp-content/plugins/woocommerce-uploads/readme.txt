=== WooCommerce Uploads ===
Contributors: wpfortune
Tags: woocommerce, upload, files
Requires at least: 3.8
Tested up to: 4.9
Stable tag: 1.4.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Let customers upload files easily in your WooCommerce webshop.

== Description ==

The WooCommerce Uploads plugin allows customers to upload one or more files for products a customer has ordered.
Once an order is processed, for each order an upload button will appear on the order overview page under 'My account'.
On the order detail page a customer can upload their files easily for their ordered products.

== Installation ==

1. Make sure WooCommerce is installed.
1. Download the .zip file.
1. Upload the entire 'woocommerce-uploads' directory into the '/wp-content/plugins/' directory.
1. Activate the plugin through the 'Plugins' menu in WordPress.
1. Configure the plugin by going to WooCommerce -> Uploads (see 'Getting started' on this page for more information).
1. Enter the license key for this plugin in our WP Fortune plugin to get support and updates.


== Frequently Asked Questions ==

For the WooCommerce Uploads FAQ, please check the Frequently Asked Questions section in our [helpdesk](https://wpfortune.com/documentation/plugins/woocommerce-uploads/)

== Changelog ==

= 2017.11.08 - 1.4.1 =
* Several fixes for new plupload version in WP 4.9
* Fixed variation show notice on upload box array

= 2017.10.18 - 1.4.0 =
* Made some non-translatable texts translatable
* Added Danish translation
* Updated Dutch translation
* Updated German translation
* Fixed More settings styling in admin (width)
* Fixed support links
* Fixed fatal bug on zip download all function

= 2017.09.27 - 1.3.9 =
* Admin order uploads improvements when deleting files
* Zip files created from admin are now temporarily stored in upload path and deleted after download
* Fixed variation display on order detail page in admin
* Fixed deprecated cart url function for Uploads Before Add-on
* Tested for WC 3.2

= 2017.09.01 - 1.3.8 =
* Get image fix
* Variation display made compatible for future WooCommerce releases

= 2017.04.26 - 1.3.7 =
* Added function for better backward compatibility for WC 3.0 < (getting order data)
* Fixed user_id in user order check on customer delete
* Fixed variation notice
* Fixed / new styling for admin settings

= 2017.04.07 - 1.3.6 =
* Fixed admin cron notifications (retreiving orders by order status)

= 2017.04.03 - 1.3.5 =
* Several adjustments and implementation of backward compatibility for WC 3.0

= 2016.10.06 - 1.3.4 =
* Better error handling when using ajax upload
* Translation fixes
* Fixed bug in admin display when deleting one file on frontend with multiple upload boxes

= 2016.06.08 - 1.3.3 =
* Added upload box amount filter (wpf_umf_product_max_uploads)
* Fixed several notices

= 2016.05.09 - 1.3.2 =
* Added support voor WooCommerce 2.6
* Replaced my orders template with new my orders table hook
* Fixed jQuery UI mixed content warning when using plugin on websites with SSL enabled
* Fixed image validation when using chunking upload and image exceeds max chunk size
* Fixed order item meta deprecated function
* Fixed several notices

= 2016.04.12 - 1.3.1 =
* Added additional hooks

= 2016.03.24 - 1.3.0 =
* Added additional hooks
* Added extra text when upload is complete, but not processed yet

= 2016.01.08 - 1.2.9 =
* Added: Message on default upload set page in admin
* Fix: Zip download all action moved
* Fix: Several small bug fixes
* Updated dutch translation

= 2015.12.08 - 1.2.8 =
* Fix: Bug in my account template: shows approved when file is deleted
* Fix: Made compatible with WC 2.5

= 2015.09.24 - 1.2.7 =
* Added: Order id to wpf_umf_filename filter
* Tweak: Products at order in admin are not displayed anymore when no files are uploaded

= 2015.08.28 - 1.2.6 =
* Added: Hook after upload boxes
* Fix: Resetting uploader instance for mobile after upload

= 2015.08.11 - 1.2.5 =
* Fix: Error on recent orders template

= 2015.07.17 - 1.2.4 =
* Added: Support for uploads depending quantity per product
* Tweak: Better support for multisite

= 2015.07.03 - 1.2.3 =
* Added: Filter to choose own filenames
* Fix: Visibility of uploadset buttons in admin on certain themes / plugins

= 2015.04.28 - 1.2.2 =
* Added: Description for file types for upload set in admin
* Fix: Permission problem on delete files in admin

= 2015.04.24 - 1.2.1 =
* Added: Possibility to delete files in admin

= 2015.04.14 - 1.2.0 =
* Tweak: Default upload dir more accurate now
* Fix: Some small fixes
* Fix: Translation of status on order overview on my account page

= 2015.04.10 - 1.1.9 =
* Fix: Some notices when HTML uploads is enabled
* Fix: Possible bug where get_post_meta does not always returns serialized array
* Tweak: Better client-side check for max uploads
* Tweak: Disable browse/upload button on ajax upload when uploading or max uploads is reached
* Added: Client-side file extension check on ajax upload

= 2015.03.12 - 1.1.8 =
* Fixed problem where'show varations' not saved correctly when adding a new upload box

= 2015.01.29 - 1.1.7 =
* Fixed problem with zip files

= 2015.01.26 - 1.1.6 =
* Added: Download all button for single order in admin (zip)
* Fix: Small bug when no uploads are needed (on variation-level)
* Fix: Bug when creating image / download urls
* Fix: Several notices

= 2014.12.17 - 1.1.5 =
* Added: Hook 'wpf_upload_complete' added when file is successfully uploaded
* Fix: Warning when no products are available for an order

= 2014.11.06 - 1.1.4 =
* Fix: Typo in approve e-mail to customer

= 2014.10.20 - 1.1.3 =
* Added: Support for dynamic variations
* Added: Check all checkbox to approve all order uploads at once
* Fix: When deleting file directly after ajax upload, reset upload limit check

= 2014.10.10 - 1.1.2 =
* Added: Possibility to let customers download their own files

= 2014.10.02 - 1.1.1 =
* Fix: Fixed small bug using the AJAX uploader preventing further uploads when an error occurs

= 2014.10.01 - 1.1.0 =
* Added: Posibility to show upload boxes only for certain product variations
* Added: Filter to customize 'View cart' button when using the Uploads Before Add-On
* Fix: Fixed small bug when using slashes in custom messages on the settings page

= 2014.09.25 - 1.0.9 =
* Fix: Error with upload boxes when using Upload Before Add-on with the HTML uploader
* Tweak: Start upload button is not visible now when Autostart is enabled

= 2014.09.15 - 1.0.8 =
* Fix: Made small changes to updater to work correctly with WP Fortune plugin v1.0.4 and above
* Fix: Disabling styling in settings works correctly now

= 2014.09.12 ' 1.0.7 =
* Fix: Fixed small bug on My account page when using WooCommerce 2.2.2

= 2014.09.10 - 1.0.6 =
* Fix: Uploads are now processed correctly when using SSL (https) on admin page

= 2014.09.09 - 1.0.5 =
* Fix: Plugin is now compatible with https version of wpfortune.com

= 2014.09.08 - 1.0.3 =
* Fix: Order numbers are now used in e-mail overview if 'order number' option is enabled
* Added: New option to start uploading automatically when files are added
* Added: Possibility to preserve filename in uploaded filename

= 2014.09.03 - 1.0.2 =
* Fix: Image check function now supports PHP 5.2 and lower
* Fix: PNG thumbnails can have transparent background now
* Fix: Uploads now working correctly when having 'Order number' selected as directory name
* Added: Support for WooCommerce Uploads Restore Settings plugin
* Added: Possibility to enable upload paths outside root directory

= 2014.09.02 - 1.0.1 =
* Fix: You can now choose more than 10 uploads per upload box

= 2014.09.01 - 1.0.0 =
* Stable release of WooCommerce Uploads
