<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'WCP_Email_Customer_Processing_Order_Printty2', false ) ) :

/**
 * Customer On-hold Order Email.
 *
 * An email sent to the customer when a new order is on-hold for.
 *
 * @class       WC_Email_Customer_On_Hold_Order
 * @version     2.6.0
 * @package     WooCommerce/Classes/Emails
 * @author      WooThemes
 * @extends     WC_Email
 */
class WCP_Email_Customer_Processing_Order_Printty2 extends WC_Email {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id             = 'customer_processing_order_printty2';
		$this->customer_email = true;
		$this->title          = __( 'Ordered Printty2 ', 'woocommerce-printty2-integration' );
		$this->description    = __( 'This is an order notification sent to customers containing order details after an order is placed processing order printty2.', 'woocommerce-printty2-integration' );
		$this->template_html  = 'emails/customer-processing-order-printty2.php';
		$this->template_plain = 'emails/plain/customer-processing-order-printty2.php';
		$this->placeholders   = array(
			'{site_title}'   => $this->get_blogname(),
			'{order_date}'   => '',
			'{order_number}' => '',
		);

		$this->template_base = WCP2I()->plugin_path() . '/templates/';
		#$this->template_base = "woocommerce-printty2-integration";

		// Call parent constructor
		parent::__construct();
	}

	/**
	 * Get email subject.
	 *
	 * @since  3.1.0
	 * @return string
	 */
	public function get_default_subject() {
		return __( '{site_title}{order_date} ご注文の製造を開始いたします。', 'woocommerce-printty2-integration' );
	}

	/**
	 * Get email heading.
	 *
	 * @since  3.1.0
	 * @return string
	 */
	public function get_default_heading() {
		return __( 'ご注文の製造開始のご連絡', 'woocommerce-printty2-integration' );
	}

	/**
	 * Trigger the sending of this email.
	 *
	 * @param int $order_id The order ID.
	 * @param WC_Order $order Order object.
	 */
	public function trigger( $order_id, $order = false ) {
		$this->setup_locale();

		if ( $order_id && ! is_a( $order, 'WC_Order' ) ) {
			$order = wc_get_order( $order_id );
		}

		if ( is_a( $order, 'WC_Order' ) ) {
			$this->object                         = $order;
			$this->recipient                      = $this->object->get_billing_email();
			$this->placeholders['{order_date}']   = wc_format_datetime( $this->object->get_date_created() );
			$this->placeholders['{order_number}'] = $this->object->get_order_number();
		}

		if ( $this->is_enabled() && $this->get_recipient() ) {
			$this->send( $this->get_recipient(), $this->get_subject(), $this->get_content(), $this->get_headers(), $this->get_attachments() );
		}

		$this->restore_locale();
	}

	/**
	 * Get content html.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_html() {
		return wc_get_template_html( $this->template_html, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => false,
			'email'         => $this,
		), '', $this->template_base );
	}

	/**
	 * Get content plain.
	 *
	 * @access public
	 * @return string
	 */
	public function get_content_plain() {
		return wc_get_template_html( $this->template_plain, array(
			'order'         => $this->object,
			'email_heading' => $this->get_heading(),
			'sent_to_admin' => false,
			'plain_text'    => true,
			'email'			=> $this,
			'',
		), '', $this->template_base );
	}
}

endif;

return new WCP_Email_Customer_Processing_Order_Printty2();
