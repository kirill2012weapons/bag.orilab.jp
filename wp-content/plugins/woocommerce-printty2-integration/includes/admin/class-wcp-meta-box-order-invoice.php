<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * WCP_Meta_Box_Order_Invoice Class.
 */
class WCP_Meta_Box_Order_Invoice {

	public static function save(){
		$order = new WCP_Order(wc_get_order( absint( $_POST['order_id'] )));

		wcp2i()->log(print_r($_FILES, true));
		if (isset($order) && isset( $_FILES['_invoice_file'] ) ) {
			return $order->set_invoice($_FILES['_invoice_file']);
		}

		return false;
	}

	public static function output( $post ) {
		global $theorder;

		if ( ! is_object( $theorder ) ) {
			$theorder = wc_get_order( $post->ID );
		}
		$order = new WCP_Order($theorder);

		if( $order->exists_invoice()){
			$action = array(
				'type'  => "woocommerce_printty2_delete_order_invoice",
				'name' => __( 'Delete Invoice', 'woocommerce-printty2-integration' ),
			);
		}else{
			$action = array(
				'type'  => "woocommerce_printty2_upload_order_invoice",
				'name' => __( 'Register Invoice', 'woocommerce-printty2-integration' ),
			);
		}

?>
<div class="panel-wrap woocommerce">
	<div id="printty2_status_data" class="panel">
		<div>
			<?php if( !$order->exists_invoice() ){ ?>
			<input type="file" id="_invoice_file" style="inherit">
			<?php }else{ ?>
			<div>
				<a href="<?php echo $order->get_invoice_url() ?>" >請求書</a>
			</div>
			<?php } ?>
			<div>
			<a style="margin:5px" class="button Printty2 submit" data-order-id="<?php echo $order->get_id(); ?>"  data-type="<?php echo $action['type']?>"  href="javascript:void(0)" ><?php echo $action['name'] ?></a>
			</div>
		</div>
		<div class="clear"></div>
	</div>
</div>
<?php
	}
}

new WCP_Meta_Box_Order_Invoice();
