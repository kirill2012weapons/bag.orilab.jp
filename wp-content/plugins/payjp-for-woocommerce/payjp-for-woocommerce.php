<?php
/*
Plugin Name: PAY.JP for WooCommerce
Plugin URI: 
Description: Add PAY.JP Payment Gateways for WooCommerce.
Version: 1.0.1
Author: AND NEXT
Author URI: http://and-next.co.jp
License: GPL
*/


define( 'WCPAYJP_Version'  , '1.0.0' );
define( 'WCPAYJP_Name'     , 'payjp' );
define( 'WCPAYJP_Dir_Name' , dirname( plugin_basename( __FILE__ ) ) );
define( 'WCPAYJP_Dir'      , plugin_dir_path( __FILE__ ) );
define( 'WCPAYJP_Url'      , plugin_dir_url( __FILE__ ) );



function payjp_woocommerce_notice()
{
	
	echo '<div class="error">';
		echo sprintf( __( 'PAY.JP for Woocommerce を利用するには %s をインストールしてください。', WCPAYJP_Name ), '<a href="http://wordpress.org/extend/plugins/woocommerce/">WooCommerce</a>' );
	echo '</div>';
	
}


function payjp_woocommece_add_gateway( $methods )
{
	
	$methods[] = 'WC_PAYJP_Payment_Gateway';
	return $methods;
	
}


function payjp_payment_gateway_load()
{
	
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		add_action( 'admin_notices', 'payjp_woocommerce_notice' );
		return;
	}
	
	
	require_once WCPAYJP_Dir.'third_party/payjp/init.php';
	
	
	require_once WCPAYJP_Dir . 'class-wc-gateway-payjp.php';
	
	
	add_filter( 'woocommerce_payment_gateways', 'payjp_woocommece_add_gateway' );
	
}
add_action( 'plugins_loaded', 'payjp_payment_gateway_load', 0 );



function payjp_action_links( $links )
{
	
	$settings = array(
		'settings' => sprintf(
			'<a href="%s">%s</a>',
			admin_url( 'admin.php?page=wc-settings&tab=checkout&section=' . WCPAYJP_Name ),
			__( 'PAY.JP の設定', WCPAYJP_Name )
		)
	);
	return array_merge( $settings, $links );
	
}
add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), 'payjp_action_links' );


/**
 * Display saved cards
 */
function payjp_render_credit_cards()
{
	$gateway = new WC_PAYJP_Payment_Gateway();
	if ( ! is_user_logged_in() | 'no' == $gateway->enabled ) {
		return;
	}
		
	$card_list = array();
	$user_customer_id = $user_customer_card_list = '';
	
	
	$user_customer_id        = get_user_meta( get_current_user_id(), 'payjp_customer_id', true );
	
	$user_customer_card_list = get_user_meta( get_current_user_id(), 'payjp_customer_card_list', true );
	
	if ( $user_customer_card_list ) :
		
		foreach( $user_customer_card_list as $fingerprint => $card_id ) :
		
			$card_info = $gateway->retrieve_card( $user_customer_id, $card_id );
			
			if ( empty( $card_info ) ) {
				
				
				
			} else {
				
				$card_list[$fingerprint] = array(
					'type'         => $card_info->brand,
					'last4_card'   => "••••••••••••" . $card_info->last4,
					'card_exp'     => $card_info->exp_month . "/" . $card_info->exp_year,
					'created_date' => date('m/d/y', $card_info->created),
				);
				
			}
			
		endforeach;
		
	endif;
?>

<div class="woocommerce-payjp-card-list">
	
	<?php if ( !empty($card_list) ): ?>
	
		<div>
		
			<h2><?php _e('お客様の登録済カード', WCPAYJP_Name); ?></h2>
			
			<table class="shop_table shop_table_responsive payjp_user_cards">
			
				<thead>
					<tr>
						<th class="card-created"><span class="nobr"><?php _e('登録日', WCPAYJP_Name); ?></span></th>
						<th class="card-brand"><span class="nobr"><?php _e('ブランド', WCPAYJP_Name); ?></span></th>
						<th class="card-last4"><span class="nobr"><?php _e('カードの下4桁', WCPAYJP_Name); ?></span></th>
						<th class="card-delete"><span class="nobr"><?php _e('削除', WCPAYJP_Name); ?></span></th>
					</tr>
				</thead>
				
				<tbody>
				<?php if ( $card_list ) : ?>
						
					<?php foreach( $card_list as $key => $item ) : ?>
						
						<tr class="card">
							<td class="card-created">
								<?php echo  $item['created_date']; ?>
							</td>
							<td class="card-brand">
								<?php echo  $item['type']; ?>
							</td>
							<td class="card-last4">
								<?php echo  $item['last4_card']; ?>
							</td>
							<td class="card-delete">
								<div class="button payjp-do-delete-card" data-card-key="<?php echo $key; ?>"><?php _e('削除する', WCPAYJP_Name); ?></div>
							</td>
						</tr>
						
					<?php endforeach; ?>
					
				<?php else: ?>
					
					<tr>
						<td colspan="4">
							<?php _e('登録されているカードはございません。', WCPAYJP_Name ); ?>
						</td>
					</tr>
						
				<?php endif; ?>
				</tbody>
			
			</table>
		
		</div>
	
	<?php endif; ?>
	
</div>
<script type="text/javascript">
jQuery(function(){
	
	jQuery(document.body).on('click', '.payjp-do-delete-card', (function(event) {
		event.preventDefault();
		
		var _this      = jQuery(this);
		var _thisTR    = _this.closest('tr.card');
		var _ThisTable = _this.closest('table');
		var _cardKey   = _this.data('card-key');
	
		jQuery.ajax({
			url: '<?php echo admin_url('admin-ajax.php'); ?>',
			method: 'POST',
			data: {
				'action'   : 'payjp_delete_card_frontend',
				'card_key' : _cardKey
			},
			dataType: 'html',
			beforeSend: function() {
				_ThisTable.block({message: null, overlayCSS: {background: '#fff url(<?php echo WCPAYJP_Url . 'include/assets/img/ajax-loader.gif'; ?>) no-repeat center', backgroundSize: '16px', opacity: 0.6, cursor:'none'}});
			},
			complete: function() {
				_ThisTable.unblock();
			},
			success: function(data) {
				_thisTR.html('<td colspan="4">' + data + '<td>');
			}
		});
	}));
});
</script>
<?php
}
add_action( 'woocommerce_before_my_account', 'payjp_render_credit_cards' );
?>