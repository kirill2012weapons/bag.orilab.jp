<?php
/**
 * Customer processing order printty2 email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
≪ご注文番号: <?= $order->get_id(); ?>≫<br/>
<br/>
<?= $order->get_formatted_billing_full_name() ?>様<br/>
<br/>
この度は、【オリジナルスマホケースラボ】をご利用いただき<br>
誠にありがとうございます。<br>
ご注文いただきました商品は、本日製作工場へ制作依頼をいたしました。<br>
制作依頼完了後、ご注文商品の手配を致します。<br>
<br>
■発送完了までの流れ<br>
1.ご注文完了 → 2.工場へ制作依頼【今ここ】 → 3.制作→ 4.発送<br>
<br>
《ご注意》 **********************************************************<br>
在庫切れやデザイン不備が発覚した場合、ご連絡メールをお送りします。<br>
予めご了承くださいませ。<br>

<hr color="#f0f0f0" />
<?php
/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );
?>
<hr color="#f0f0f0" />
■製作依頼<br>
<br>
通常商品 → 本日より4営業日内に出荷予定<br>
<br>
天災などの理由により商品の配送が不可能となった場合、<br>
ご注文の再製作及びキャンセルし返金対応させて頂く場合がございます。<br>
尚、なんらかの事情によりお届けが難しくなった場合には、当サイトよりご連絡さしあげます。<br>
<br>
また、製作工場側にてお客様のデザインに不備などを発見した場合や<br>
商品のメーカー在庫切れなどが起こった場合など、<br>
サポートデスクよりお客様にご連絡させていただくこともございます。<br>
お客様からご回答がない場合、製作を進められない可能性もありますので<br>
連絡があった場合にはご回答をよろしくお願いいたします。<br>
<br>
ご依頼商品出荷依頼を出しましたらメールにて出荷完了のご報告をさせていただきます。<br>

それでは今後ともオリジナルスマホケースラボをよろしくお願いいたします。<br>
<br>
その他ご注文に関する不明点、疑問点などお問い合わせに関しましては<br>
以下のお問い合わせ窓口よりご連絡ください。<br>
<br>
お問い合わせの前にこちらもご確認ください。<br>
FAQ（よくある質問）<a href="https://original-smaphocase.com/info/faq">https://original-smaphocase.com/info/faq</a><br>
<br>
このEメールアドレスは、配信専用です。このメッセージに返信しないようお願いいたします。<br>
<br>
―オリラボ―――――――――――――――――――――<br>
URL（PC）： <a href="https://original-smaphocase.com/">https://original-smaphocase.com/</a><br>
お問い合わせ（PC）： <a href="https://original-smaphocase.com/contact">https://original-smaphocase.com/contact</a><br>
電話（月〜金9時から18時まで）： 0120-570-150 （姉妹サイトTMIXサポートデスクで対応致します。）<br>
運営会社 ： 株式会社spice life<br>
―――――――――――――――――――――――――<br>
</p>

<?php

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
