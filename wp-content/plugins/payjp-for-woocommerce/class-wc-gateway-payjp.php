<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


class WC_PAYJP_Payment_Gateway extends WC_Payment_Gateway {

	
	/**
	 * Constructor
	 */
	public function __construct()
	{
		
		$this->id                 = WCPAYJP_Name;
		$this->method_title       = __( 'PAY.JP', WCPAYJP_Name );
		$this->method_description = __( 'PAY.JP でのお支払いを可能にします。', WCPAYJP_Name );
		$this->has_fields         = true;
		
		$this->supports = array(
			'products',
			'refunds',
		);
		
		// Load the settings
		$this->init_form_fields();
		$this->init_settings();
		
		// Get settings
		$this->title              = $this->get_option( 'title' );
		$this->description        = $this->get_option( 'description' );
		$this->instructions       = $this->get_option( 'instructions', $this->description );
		/*
		$this->enable_for_methods = $this->get_option( 'enable_for_methods', array() );
		*/
		
		$this->testmode           = $this->get_option( 'testmode' );
		
		$this->testPublishableKey = $this->get_option( 'test_public_key' );
		$this->livePublishableKey = $this->get_option( 'live_public_key' );
		
		$this->testSecretKey      = $this->get_option( 'test_secret_key' );
		$this->liveSecretKey      = $this->get_option( 'live_secret_key' );
		
		if ( $this->testmode == 'no' ) {
			$this->publishable_key    = $this->livePublishableKey;
			$this->secret_key         = $this->liveSecretKey;
		} else {
			$this->publishable_key    = $this->testPublishableKey;
			$this->secret_key         = $this->testSecretKey;
		}
		
		if ( version_compare( WOOCOMMERCE_VERSION, '2.2.8', '>=' ) ) {
			add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, array( $this, 'process_admin_options' ) );
		} else {
			add_action( 'woocommerce_update_options_payment_gateways', array( $this, 'process_admin_options' ) );
		}
		
		add_action( 'woocommerce_thankyou_' . $this->id, array( $this, 'thankyou_page' ) );
		
		add_action( 'wp_ajax_payjp_delete_card_frontend', array( $this, 'delete_card_frontend' ) );
		
		$this->card_status        = '';
		
		$this->user_id            = get_current_user_id();
		
		$this->order              = '';
		
		$this->order_id           = '';
		
		$this->payjs_payment_id   = '';
		
		if ( ! $this->is_valid_for_use() ) {
			$this->enabled = 'no';
		}
		
	}
	
	
	/**
	 * Init payment gateway form fields
	 */
	public function init_form_fields()
	{
		
		$this->form_fields = array(
			'enabled' => array(
				'type'        => 'checkbox',
				'title'       => __( 'Enable/Disable', 'woocommerce' ),
				'label'       => __( 'PAY.JP を有効にします', WCPAYJP_Name ),
				'default'     => 'yes'
			),
			'title' => array(
				'title'       => __( 'Title', 'woocommerce' ),
				'type'        => 'text',
				'description' => __( 'Payment method description that the customer will see on your checkout.', 'woocommerce' ),
				'default'     => __( 'PAY.JP', WCPAYJP_Name ),
				'desc_tip'    => true,
			),
			'description' => array(
				'title'       => __( 'Description', 'woocommerce' ),
				'type'        => 'textarea',
				'description' => __( 'Payment method description that the customer will see on your website.', 'woocommerce' ),
				'default'     => __( 'クレジットカードでお支払い', WCPAYJP_Name ),
				'desc_tip'    => true,
			),
			'instructions' => array(
				'title'       => __( 'Instructions', 'woocommerce' ),
				'type'        => 'textarea',
				'description' => __( 'Instructions that will be added to the thank you page.', 'woocommerce' ),
				'default'     => '',
				'desc_tip'    => true,
			),
			'testmode' => array(
				'type'        => 'checkbox',
				'title'       => __( 'テストモード', WCPAYJP_Name ),
				'label'       => __( 'テストモードを有効にします', WCPAYJP_Name ),
				'default'     => 'no'
			),
			'test_public_key' => array(
				'type'        => 'text',
				'title'       => __( 'テスト公開鍵', WCPAYJP_Name ),
				'default'     => __( 'pk_test_xxxxxxxxxxxxxxxxxxxxxxxx', WCPAYJP_Name )
				),
			'test_secret_key' => array(
				'type'        => 'text',
				'title'       => __( 'テスト秘密鍵', WCPAYJP_Name ),
				'default'     => __( 'sk_test_xxxxxxxxxxxxxxxxxxxxxxxx', WCPAYJP_Name )
			),
			'live_public_key' => array(
				'type'        => 'text',
				'title'       => __( '本番公開鍵', WCPAYJP_Name ),
				'default'     => __( 'pk_live_xxxxxxxxxxxxxxxxxxxxxxxx', WCPAYJP_Name )
			),
			'live_secret_key' => array(
				'type'        => 'text',
				'title'       => __( '本番秘密鍵', WCPAYJP_Name ),
				'default'     => __( 'sk_live_xxxxxxxxxxxxxxxxxxxxxxxx', WCPAYJP_Name )
			),
		);
	
	}
	
	
	/**
	 * Check if this gateway is enabled and available in the user's country
	 *
	 * @return bool
	 */
	public function is_valid_for_use() {
		return in_array( get_woocommerce_currency(), apply_filters( 'woocommerce_payjp_supported_currencies', array( 'JPY' ) ) );
	}
	
	
	/**
	 * Admin Panel Options
	 * - Options for bits like 'title' and availability on a country-by-country basis
	 *
	 * @since 1.0.0
	 */
	public function admin_options() {
		if ( $this->is_valid_for_use() ) {
			parent::admin_options();
		} else {
			?>
			<div class="inline error"><p><strong><?php _e( 'Gateway Disabled', 'woocommerce' ); ?></strong>: <?php _e( 'PAY.JP does not support your store currency.', CPAYJP_Name ); ?></p></div>
			<?php
		}
	}
	
	
	protected function getRequestData()
	{
	
		if ( $this->card_status == 'new' or $this->card_status == 'add' or $this->card_status == 'guest' ) {
			
			if ( !isset($_POST['card_number']) || !isset($_POST['card_exp_month']) || !isset($_POST['card_exp_year']) || !isset($_POST['card_cvc']) || !isset($_POST['card_name']) ) {
				
				$message = __('カード情報の記入漏れをご確認ください', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
				
			}

			$card_number = '';

			if( strpos( $_POST['card_number'] , ' ' ) !== false ) {
				
				$tmp_number = str_replace( ' ' , '' , $_POST['card_number'] );
				$card_numbers = str_split( $tmp_number );
				
				foreach( $card_numbers as $key => $cn ) {
					
					$cn = htmlspecialchars( $cn );
					$card_number .= intval( $cn );
					
				}
			
			}
			
			return array(
				"amount"      => intval($this->order->get_total()),
				"currency"    => strtolower( 'jpy' ),
				"card"        => array(
					"number"    => $card_number,
					"exp_month" => intval( $_POST['card_exp_month'] ),
					"exp_year"  => intval( $_POST['card_exp_year'] ),
					"cvc"       => intval( $_POST['card_cvc'] ),
					"name"      => strip_tags( $_POST['card_name'] )
				)
			);
		
		} elseif ( $this->card_status == 'saved' ) {
			
			if ( !isset($_POST['saved_card_key']) ) {
				
				$message = __('識別キーが正しく送信されませんでした（No saved card key）', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
				
			}
			$saved_card_key = strip_tags( $_POST['saved_card_key'] );
			
			$customer_card_list = get_user_meta( $this->user_id, 'payjp_customer_card_list', true );
			if ( empty($customer_card_list) || !array_key_exists( $saved_card_key, $customer_card_list ) ) {
				
				$message = __('登録済のカード情報が見つかりませんでした（No saved card）', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
				
			}
			
			return array(
				"amount"      => intval($this->order->get_total()),
				"currency"    => strtolower( 'jpy' ),
				"card"        => $customer_card_list[$saved_card_key]
			);
				
		}
	
	}
	
	
	/**
	 * Payment form on checkout page
	 */
	public function payment_fields()
	{
		
		if ( $description = $this->get_description() ) {
			echo wpautop( wptexturize( $description ) );
		}
		
		include_once('include/payment-field.php');
	
	}

	
	protected function send_to_payjp()
	{
		
		
		if ( empty($this->card_status) ) {
			$this->card_status = esc_attr( $_POST['card_status'] );
		}
		
		
		if ( $this->card_status != 'new' && $this->card_status != 'saved' && $this->card_status != 'add' && $this->card_status != 'guest' ) {
		
			$message = __('カードステータスが送信されませんでした（No card status）', WCPAYJP_Name );
			wc_add_notice( $message, 'error' );
			return false;
			
		}
		
		
		$data = $this->getRequestData();
		if ( empty($data) ) {
			
			return false;
			
		}
		
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
		
		
		if ( !empty( $this->user_id ) && $this->card_status == 'new' ) {
			
			// 顧客作成
			try {
				
				$new_customer = \Payjp\Customer::create(array(
					"card"        => $data['card'],
					"metadata"    => array(
						"user_id" => $this->user_id
					)
				));
			
			} catch(\Payjp\Error\Card $e) {
				// Since it's a decline, \Payjp\Error\Card will be caught
				$body = $e->getJsonBody();
				$err  = $body['error'];
				
				/*
				print('Status is:' . $e->getHttpStatus() . "\n");
				print('Type is:' . $err['type'] . "\n");
				print('Code is:' . $err['code'] . "\n");
				// param is '' in this case
				print('Param is:' . $err['param'] . "\n");
				print('Message is:' . $err['message'] . "\n");
				*/
				
				$message = $err['message'];
				wc_add_notice( $message, 'error' );
				return false;
			
			} catch (\Payjp\Error\InvalidRequest $e) {
				// Invalid parameters were supplied to Payjp's API
				
				$message = __('顧客作成エラー（Invalid parameters）', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
			
			} catch (\Payjp\Error\Authentication $e) {
				// Authentication with Payjp's API failed
				
				$message = __('顧客作成エラー（Authentication failed）', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
			
			} catch (\Payjp\Error\ApiConnection $e) {
				// Network communication with Payjp failed
				
				$message = __('顧客作成エラー（Network failed）', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
			
			} catch (\Payjp\Error\Base $e) {
				// Display a very generic error to the user, and maybe send
				// yourself an email
				
				$message = __('顧客作成エラー（Generic error）', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
			
			} catch (Exception $e) {
				// Something else happened, completely unrelated to Payjp
				
				$message = __('顧客作成エラー（Error）', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;
			
			}
			
			if ( $new_customer->id ) {
				
				$new_customer_id = $new_customer->id;
				
				$customer_id = get_user_meta( $this->user_id, 'payjp_customer_id', true );
				if ( empty($customer_id) ) {
					
					update_user_meta( $this->user_id, 'payjp_customer_id', $new_customer_id );
				
				}
				
				if ( $new_customer->cards ) {
					
					$new_customer_card = $new_customer->cards->data[0];
					
					$customer_card_list = get_user_meta( $this->user_id, 'payjp_customer_card_list', true );
					if ( empty($customer_card_list) ) {
						
						update_user_meta( $this->user_id, 'payjp_customer_card_list', array( $new_customer_card->fingerprint => $new_customer_card->id ) );
					
					}
					
				}
				
			}
			
		}
		
		
		
		
		if ( !empty( $this->user_id ) && $this->card_status == 'add' ) {
			
			$customer_id = get_user_meta( $this->user_id, 'payjp_customer_id', true );
			if ( empty( $customer_id ) ) {
				$message = __('顧客情報が見つかりませんでした', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;	
			}
			
			$new_card_info = $this->add_card($customer_id, $data['card']);
			if ( empty( $new_card_info ) ) {
				$message = __('カードを正常に追加できませんでした', WCPAYJP_Name );
				wc_add_notice( $message, 'error' );
				return false;	
			}
			
			$customer_card_list = get_user_meta( $this->user_id, 'payjp_customer_card_list', true );
			if ( empty($customer_card_list) ) {
					
				update_user_meta( $this->user_id, 'payjp_customer_card_list', array( $new_card_info->fingerprint => $new_card_info->id ) );
				
			} else {
				
				$customer_card_list[$new_card_info->fingerprint] = $new_card_info->id;
				update_user_meta( $this->user_id, 'payjp_customer_card_list', $customer_card_list );
			
			}
			
		}
		
		
		
		
		// 決済実行
		try {
			
			if ( $this->card_status == "guest" ) {
					
				$charge_result = \Payjp\Charge::create(array(
					"amount"       => $data['amount'],
					"currency"     => $data['currency'],
					"card"         => $data['card'],
					"metadata"     => array(
						"order_id" => $this->order_id
					)
				));
					
			} elseif ( $this->card_status == 'new' ) {
				
				if ( $new_customer->id ) {
					
					$charge_result = \Payjp\Charge::create(array(
						"amount"       => $data['amount'],
						"currency"     => $data['currency'],
						"customer"     => $new_customer->id,
						"metadata"     => array(
							"order_id" => $this->order_id
						)
					));
					
				} else {
					
					$message = __('顧客情報の保存段階で不具合がありました（No new customer id）', WCPAYJP_Name );
					wc_add_notice( $message, 'error' );
					return false;
					
				}
				
			} elseif ( $this->card_status == 'add' ) {
				
				if ( $new_card_info->id ) {
					
					$customer_id = get_user_meta( $this->user_id, 'payjp_customer_id', true );
					if ( $customer_id ) {	
						
						// Change default card
						$this->change_default_card($customer_id, $new_card_info->id);
						
						$charge_result = \Payjp\Charge::create(array(
							"amount"       => $data['amount'],
							"currency"     => $data['currency'],
							"customer"     => $customer_id,
							"metadata"     => array(
								"order_id" => $this->order_id
							)
						));
					
					} else {
						
						$message = __('顧客情報が見つかりませんでした（No customer id）', WCPAYJP_Name );
						wc_add_notice( $message, 'error' );
						return false;
						
					}
					
				} else {
					
					$message = __('カード情報の保存段階で不具合がありました（No new card id）', WCPAYJP_Name );
					wc_add_notice( $message, 'error' );
					return false;
					
				}
				
			} elseif ( $this->card_status == 'saved' ) {
				
				$customer_id = get_user_meta( $this->user_id, 'payjp_customer_id', true );
				if ( $customer_id ) {	
					
					// Change default card
					$this->change_default_card($customer_id, $data['card']);
					
					$charge_result = \Payjp\Charge::create(array(
						"amount"       => $data['amount'],
						"currency"     => $data['currency'],
						"customer"     => $customer_id,
						"metadata"     => array(
							"order_id" => $this->order_id
						)
					));
				
				} else {
					
					$message = __('顧客情報が見つかりませんでした（No customer id）', WCPAYJP_Name );
					wc_add_notice( $message, 'error' );
					return false;
					
				}
				
			}
			
		} catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			wc_add_notice( $message, 'error' );
			return false;
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __('決済エラー（Invalid parameters）', WCPAYJP_Name );
			wc_add_notice( $message, 'error' );
			return false;
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __('決済エラー（Authentication failed）', WCPAYJP_Name );
			wc_add_notice( $message, 'error' );
			return false;
			
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __('決済エラー（Network failed）', WCPAYJP_Name );
			wc_add_notice( $message, 'error' );
			return false;
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __('決済エラー（Generic error）', WCPAYJP_Name );
			wc_add_notice( $message, 'error' );
			return false;
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __('決済エラー（Error）', WCPAYJP_Name );
			wc_add_notice( $message, 'error' );
			return false;
		
		}
		
		
		$this->payjs_payment_id = $charge_result->id;
		
		
		return true;
			
	}
	
	
	/**
	 * Process payment.
	 *
	 * @param int $order_id
	 */
	public function process_payment( $order_id ) 
	{
		
		$this->order_id = $order_id;
		$this->order    = wc_get_order( $order_id );
		if ( isset($this->order->customer_user) && !empty($this->order->customer_user) ) {
			$customer = get_userdata($this->order->customer_user);
			$wc_create_new_customer_id = $customer->ID;
			if ( empty( $this->user_id ) ) {
				$this->user_id = $wc_create_new_customer_id;
			}
		} else {
			$this->card_status = 'guest';
		}
		
		if ($this->send_to_payjp()) {
			
			// Store reference ID in the order.
			update_post_meta( $this->order_id, 'payjs_payment_id', $this->payjs_payment_id );
			
			$this->order->add_order_note(
				sprintf(
					__( "PAY.JP でのお支払いIDは %s です。WooCommerceからの返金処理は一度限り有効です。", WCPAYJP_Name ),
					$this->payjs_payment_id
				)
			);
			 // Mark as on-hold (we're awaiting the payment)
			$this->order->update_status( 'processing', __( 'PAY.JPによる決済が完了しましたので', WCPAYJP_Name ) );
			
			// Reduce stock levels
			$this->order->reduce_order_stock();
			
			// Remove cart
			WC()->cart->empty_cart();
			
			// Return thankyou redirect
			return array(
				'result'    => 'success',
				'redirect'  => $this->get_return_url( $this->order )
			);
			
		}
		
	}
	
	
	/**
	 * Process refund.
	 *
	 * @since 1.6.0
	 *
	 * @param  int $order_id    Order ID
	 * @param  float $amount    Amount to refund
	 * @param  string $reason   Reason to refund
	 * @return WP_Error|boolean True or false based on success, or a WP_Error object.
	 */
	public function process_refund( $order_id, $refund_amount = null, $reason = '' )
	{

		$payjs_payment_id = get_post_meta( $order_id, 'payjs_payment_id', true );
		if ( empty( $payjs_payment_id ) ) {
			return new WP_Error( 'error', sprintf( __( '注文番号 %s のPAY.JPでのお支払いIDがみつかりません', WCPAYJP_Name ), $order_id ) );
		}
		
		$payjs_refund_amount_check = get_post_meta( $order_id, 'payjs_refund_amount_check', true );
		if ( $payjs_refund_amount_check ) {
			return new WP_Error( 'error', sprintf( __( '注文番号 %s は既に返金処理を一度行っている為、WooCommerceからの返金処理はできません', WCPAYJP_Name ), $order_id ) );
		}
		
		$order = wc_get_order( $order_id );
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
			
		try {
			
			$refoun_data = array(
				'amount'        => intval($refund_amount),
				'refund_reason' => $reason
			);
			
			$ch = \Payjp\Charge::retrieve( $payjs_payment_id );
			$ch->refund( $refoun_data );
			
			update_post_meta( $order_id, 'payjs_refund_amount', intval($refund_amount) );
			
			update_post_meta( $order_id, 'payjs_refund_amount_check', 1 );
			
			$order->add_order_note(
				sprintf(
					__( "%s 円の返金処理が完了しました。PAY.JPの仕様により、WooCommerceからの二度目の返金処理はできません", WCPAYJP_Name ),
					intval($refund_amount)
				)
			);
			
			return true;
			
		}catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			return new WP_Error( 'error', $message );
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __( '返金エラー（Invalid parameters）', WCPAYJP_Name );
			return new WP_Error( 'error', $message );
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __( '返金エラー（Authentication failed）', WCPAYJP_Name );
			return new WP_Error( 'error', $message );
		
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __( '返金エラー（Network failed）', WCPAYJP_Name );
			return new WP_Error( 'error', $message );
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __( '返金エラー（Generic error）', WCPAYJP_Name );
			return new WP_Error( 'error', $message );
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __( '返金エラー（Error）', WCPAYJP_Name );
			return new WP_Error( 'error', $message );
		
		}
		
	}
	
	public function retrieve_card($customer_id, $card_id)
	{
		
		if ( empty($customer_id) or empty($card_id) ) {
			return false;
		}
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
			
		try {
			
			$customer  = \Payjp\Customer::retrieve( $customer_id );
			$card_info = $customer->cards->retrieve( $card_id );
			
		}catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			return false;
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __('カード情報取得エラー（Invalid parameters）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __('カード情報取得エラー（Authentication failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __('カード情報取得エラー（Network failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __('カード情報取得エラー（Generic error）', WCPAYJP_Name );
			return false;
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __('カード情報取得エラー（Error）', WCPAYJP_Name );
			return false;
		
		}
			
		return $card_info;
		
	}
	
	public function add_card($customer_id, $card_data)
	{
		
		if ( empty($customer_id) or empty($card_data) ) {
			return false;
		}
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
			
		try {
			
			$customer  = \Payjp\Customer::retrieve( $customer_id );
			$card_info = $customer->cards->create( $card_data );
			
		}catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			return false;
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __('カード追加エラー（Invalid parameters）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __('カード追加エラー（Authentication failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __('カード追加エラー（Network failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __('カード追加エラー（Generic error）', WCPAYJP_Name );
			return false;
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __('カード追加エラー（Error）', WCPAYJP_Name );
			return false;
		
		}
			
		return $card_info;
		
	}
	
	public function change_default_card($customer_id, $card_id)
	{
		
		if ( empty($customer_id) or empty($card_id) ) {
			return false;
		}
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
			
		try {
			
			$customer = \Payjp\Customer::retrieve( $customer_id );
			$customer->default_card = $card_id;
			$customer->save();
			
		}catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			return false;
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __('デフォルトカード置換エラー（Invalid parameters）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __('デフォルトカード置換エラー（Authentication failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __('デフォルトカード置換エラー（Network failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __('デフォルトカード置換エラー（Generic error）', WCPAYJP_Name );
			return false;
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __('デフォルトカード置換エラー（Error）', WCPAYJP_Name );
			return false;
		
		}
			
		return true;
		
	}
	
	public function delete_card($customer_id, $card_id)
	{
		
		if ( empty($customer_id) or empty($card_id) ) {
			return false;
		}
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
			
		try {
			
			$customer = \Payjp\Customer::retrieve( $customer_id );
			$card     = $customer->cards->retrieve($card_id);
			$card->delete();
			
		}catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			return false;
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __('カード削除エラー（Invalid parameters）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __('カード削除エラー（Authentication failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __('カード削除エラー（Network failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __('カード削除エラー（Generic error）', WCPAYJP_Name );
			return false;
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __('カード削除エラー（Error）', WCPAYJP_Name );
			return false;
		
		}
			
		return true;
		
	}
	
	public function retrieve_customer($customer_id)
	{
		
		if ( empty($customer_id) ) {
			return false;
		}
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
			
		try {
			
			$customer = \Payjp\Customer::retrieve( $customer_id );
			
		}catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			return false;
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __('顧客情報取得エラー（Invalid parameters）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __('顧客情報取得エラー（Authentication failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __('顧客情報取得エラー（Network failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __('顧客情報取得エラー（Generic error）', WCPAYJP_Name );
			return false;
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __('顧客情報取得エラー（Error）', WCPAYJP_Name );
			return false;
		
		}
			
		return $customer;
		
	}
	
	public function delete_customer($customer_id)
	{
		
		if ( empty($customer_id) ) {
			return false;
		}
		
		\Payjp\Payjp::setApiKey( $this->secret_key );
			
		try {
			
			$customer = \Payjp\Customer::retrieve( $customer_id );
			$customer->delete();
			
		}catch(\Payjp\Error\Card $e) {
			// Since it's a decline, \Payjp\Error\Card will be caught
			$body = $e->getJsonBody();
			$err  = $body['error'];
			
			/*
			print('Status is:' . $e->getHttpStatus() . "\n");
			print('Type is:' . $err['type'] . "\n");
			print('Code is:' . $err['code'] . "\n");
			// param is '' in this case
			print('Param is:' . $err['param'] . "\n");
			print('Message is:' . $err['message'] . "\n");
			*/
			
			$message = $err['message'];
			return false;
		
		} catch (\Payjp\Error\InvalidRequest $e) {
			// Invalid parameters were supplied to Payjp's API
			
			$message = __('顧客削除エラー（Invalid parameters）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Authentication $e) {
			// Authentication with Payjp's API failed
			
			$message = __('顧客削除エラー（Authentication failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\ApiConnection $e) {
			// Network communication with Payjp failed
			
			$message = __('顧客削除エラー（Network failed）', WCPAYJP_Name );
			return false;
		
		} catch (\Payjp\Error\Base $e) {
			// Display a very generic error to the user, and maybe send
			// yourself an email
			
			$message = __('顧客削除エラー（Generic error）', WCPAYJP_Name );
			return false;
		
		} catch (Exception $e) {
			// Something else happened, completely unrelated to Payjp
			
			$message = __('顧客削除エラー（Error）', WCPAYJP_Name );
			return false;
		
		}
		
		return true;
		
	}
	
	
	/**
	 * Output for the order received page.
	 */
	public function thankyou_page()
	{
		if ( $this->instructions ) {
			echo wpautop( wptexturize( $this->instructions ) );
		}
	}
	
	
	public function delete_card_frontend()
	{
		
		if ( !is_user_logged_in() ) :
			_e( 'ログインしていません', WCPAYJP_Name );
			die();
		endif;
		
		
		if ( empty( $_POST['card_key'] ) ) {
			_e( 'カードキーが送信されていません', WCPAYJP_Name );
			die();
		}
		$post_card_key = esc_html( $_POST['card_key'] );
		
		
		$customer_id = get_user_meta( $this->user_id, 'payjp_customer_id', true );
		if ( empty($customer_id) ) {
			_e( '顧客情報が見つかりません', WCPAYJP_Name );
			die();
		}
		
		
		$card_list = get_user_meta( $this->user_id, 'payjp_customer_card_list', true );
		if ( empty($card_list) || !array_key_exists( $post_card_key, $card_list ) ) {
			_e( 'カード情報が見つかりません', WCPAYJP_Name );
			die();
		}
		
		$card_id = $card_list[$post_card_key];
		
		$result = $this->delete_card($customer_id, $card_id);
		
		if ( $result ) {
			
			unset( $customer_card_list[$card_key] );
			update_user_meta( $this->user_id, 'payjp_customer_card_list', $card_list );
			
			_e( '削除が完了しました', WCPAYJP_Name );
			die();
			
		} else {
			
			_e( '削除に失敗しました', WCPAYJP_Name );
			die();
			
		}

	}

}