<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @class  WCP_Printty2_Master_Base
 */
class WCP_Printty2_Master_Base {
	static public function get_master_file() {

		$path = WCP_ABSPATH . static::get_master_file_path();
		$file = new SplFileObject($path);
		$file->setFlags(SplFileObject::READ_CSV);
		return $file;
	}

	public function get_id(){
		return intval($this->id);
	}

	public function get_title(){
		return $this->title;
	}
}
