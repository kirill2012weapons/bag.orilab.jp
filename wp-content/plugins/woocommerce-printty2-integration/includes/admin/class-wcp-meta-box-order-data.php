<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * WC_Meta_Box_Order_Data Class.
 */
class WCP_Meta_Box_Order_Data {

	public function __construct() {
		add_action('woocommerce_admin_order_data_after_order_details', [$this,  'add_action_woocommerce_admin_order_data_after_shipping_address']);
	}

	public static function save( $order_id ) {
		$order = new WCP_Order(wc_get_order( $order_id )); 
		$props = array();

		$shipping_delivery_period_id = isset( $_POST['_shipping_delivery_period_id'] ) ? absint( $_POST['_shipping_delivery_period_id'] ) : 0;
		if ( $shipping_delivery_period_id !== $order->get_shipping_delivery_period_id() ) {
			$order->set_shipping_delivery_period_id($shipping_delivery_period_id);
		}

		$use_package = isset( $_POST['_use_package'] ) ? absint( $_POST['_use_package'] ) : 0;
		if ( $use_package !== $order->get_use_package() ) {
			$order->set_use_package($use_package);
		}

		$priority_type_code = isset( $_POST['_priority_type_code'] ) ? $_POST['_priority_type_code'] : '3BusinessDay';
		if ( $priority_type_code !== $order->get_priority_type_code() ) {
			$order->set_priority_type_code($priority_type_code);
		}
		$order->save();
	}

	public function add_action_woocommerce_admin_order_data_after_shipping_address( $order ){
		$order = new WCP_Order($order);

/**
		# OrderType(デザイン方法)
?>
<p class="form-field form-field-wide">
		<label for="_order_type"><?php _e('Order Type', 'woocommerce-printty2-integration' ); ?></label>
		<select id="_order_type" name="_order_type" class="wc-enhanced-select">
	<?php
			foreach ( $this->order_types() as $status => $status_name ) {
				echo '<option value="' . esc_attr( $status ) . '" ' . selected( $status, $order->get_order_type(), false ) . '>' . esc_html( $status_name ) . '</option>';
			}
	?>
		</select>
</p>
<?php
**/

		# Priority Type Code

?>
<p class="form-field form-field-wide">
	<label for="_priority_type_code"><?php _e('Priority Type', 'woocommerce-printty2-integration' ); ?></label>
	<select id="_priority_type_code" name="_priority_type_code" class="wc-enhanced-select">
<?php
		foreach ( $this->priority_type_codes() as $status => $status_name ) {
			echo '<option value="' . esc_attr( $status ) . '" ' . selected( $status, $order->get_priority_type_code(), false ) . '>' . esc_html( $status_name ) . '</option>';
		}
?>
	</select>
</p>
<?php
		# Shipping Delivery Period
?>
<p class="form-field form-field-wide">
	<label for="_shipping_delivery_period_id"><?php _e('Shipping Delivery Period', 'woocommerce-printty2-integration' ); ?></label>
	<select id="_shipping_delivery_period_id" name="_shipping_delivery_period_id" class="wc-enhanced-select">
<?php
		foreach ( $this->shipping_delivery_period_types() as $status => $status_name ) {
			echo '<option value="' . esc_attr( $status ) . '" ' . selected( $status, $order->get_shipping_delivery_period_id(), false ) . '>' . esc_html( $status_name ) . '</option>';
		}
?>
	</select>
</p>
<?php


		# Use Package
?>
<p class="form-field form-field-wide">
	<label for="_use_package"><?php _e('Use Package', 'woocommerce-printty2-integration' ); ?></label>
	<select id="_use_package" name="_use_package" class="wc-enhanced-select">
<?php
		foreach ( $this->use_package_types() as $status => $status_name ) {
			echo '<option value="' . esc_attr( $status ) . '" ' . selected( $status, $order->get_use_package(), false ) . '>' . esc_html( $status_name ) . '</option>';
		}
?>
	</select>
</p>
<?php
	}

	public function order_types(){

		$types = array();
		$types['data_submission'] = __('Data Submission', 'woocommerce-printty2-integration' );
		$types['consultation'] = __('Consultation', 'woocommerce-printty2-integration' );

		return $types;
	}

	public function shipping_delivery_period_types(){

		$types = array();
		$types[1] = __('none', 'woocommerce-printty2-integration' );
		$types[2] = __('8:00-12:00', 'woocommerce-printty2-integration' );
		$types[3] = __('12:00-16:00', 'woocommerce-printty2-integration' );
		$types[4] = __('16:00-20:00', 'woocommerce-printty2-integration' );

		return $types;
	}

	public function use_package_types(){
		$types = array();
		$types[0] = __('not use', 'woocommerce-printty2-integration' );
		$types[1] = __('use', 'woocommerce-printty2-integration' );

		return $types;
	}

	public function priority_type_codes(){
		$types = array();
		$types['3BusinessDay'] = __('3 business day', 'woocommerce-printty2-integration' );
		$types['5BusinessDay'] = __('5 business day', 'woocommerce-printty2-integration' );
		$types['7BusinessDay'] = __('7 business day', 'woocommerce-printty2-integration' );
		$types['auto'] = __('auto', 'woocommerce-printty2-integration' );
		$types['4BusinessDay'] = __('4 business day', 'woocommerce-printty2-integration' );
		$types['9BusinessDay'] = __('9 business day', 'woocommerce-printty2-integration' );

		return $types;
	}
}

new WCP_Meta_Box_Order_Data();
