��          �      �       0  e   1     �     �     �     �     �     �       Q   !  	   s     }  9   �  /  �  y   �     w  )   �     �     �     �     �       t   "  	   �     �  9   �            	                
                              Add attractive social sharing icons for Facebook, Twitter, Pinterest and Email to your product pages. Cheatin&#8217; huh? Install Storefront now Pin this product Share on Facebook Share on Twitter Share via Email Storefront Product Sharing Storefront Product Sharing requires that you use Storefront as your parent theme. WooThemes http://woothemes.com/ http://woothemes.com/products/storefront-product-sharing/ PO-Revision-Date: 2016-10-17 08:30:02+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Generator: GlotPress/2.3.0-alpha
Language: ja_JP
Project-Id-Version: Plugins - Storefront Product Sharing - Stable (latest release)
 商品ページに Facebook・Twitter・Pinterest・メール用のソーシャル共有アイコンを追加します。 間違いましたか ? Storefront を今すぐインストール この商品をピン Facebook で共有 Twitter で共有 メールで共有 Storefront Product Sharing Storefront Product Sharing を使うには、Storefront を親テーマとして使用する必要があります。 WooThemes http://woothemes.com/ http://woothemes.com/products/storefront-product-sharing/ 