jQuery( function ( $ ) {
	$( '#woocommerce-order-items' ).on('click', '.printty2-file-select', function(event){
		event.stopPropagation()
	});
	$( '#woocommerce-order-items' ).on('change', '.printty2-file-select input', function(){
		var $input = $(this);
		if (!this.files.length) {
			return;
		}

		var $parent = $input.parent();
		var $img = $parent.find('img');
		var $span = $parent.find('span');

		var file = $(this).prop('files')[0];
		var fr = new FileReader();
		fr.onload = media_upload( $input );
		fr.readAsDataURL(file);
	});

	var media_upload = function( $input ){
		return function(){
			var form = new FormData();
			form.append( 'action', 'woocommerce_printty2_save_order_item_image');
			form.append( 'order_id', $input.data('order-id'));
			form.append( 'item_id', $input.data('item-id'));
			if($input.data('item-field') == 'image'){
				form.append("_printty2_image", $input.prop('files')[0]);
			} else {
				form.append("_printty2_preview_image", $input.prop('files')[0]);
			}

			$.ajax({
				url:  woocommerce_admin_meta_boxes.ajax_url,
				data: form,
				type: 'POST',
				processData: false,
				contentType: false,
				dataType: 'html',
				success: function( response ) {
					// 無害なキャンセルアクションでリロード
					$cancel = $( '#woocommerce-order-items .cancel-action' );
					$cancel.attr('data-reload', 'true');
					$cancel.trigger('click');
				},
				error: function(){
					alert("upload failed");
				}
			});
		}
	}

	$("form").submit(function(){
		return window.confirm("更新します。");
	});

	$( '#printty2_invoice' ).on('click', 'a.submit', function(){
		$.blockUI();

		var $a = $(this);
		var $input = $('#printty2_invoice input').first();

		var form = new FormData();
		form.append( 'action', $a.data('type'));
		form.append( 'order_id', $a.data('order-id'));
		if ( $a.data('type') == 'woocommerce_printty2_upload_order_invoice' ){
			form.append("_invoice_file", $input.prop('files')[0]);
		}

		$.ajax({
			url:  woocommerce_admin_meta_boxes.ajax_url,
			data: form,
			type: 'POST',
			async: false,
			processData: false,
			contentType: false,
			dataType: 'html',
			complete: function( response ) {
				location.reload();
			}
		});
	});


});
