<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates/Emails
 * @version     2.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
<p>
≪ご注文番号: <?= $order->get_id(); ?>≫<br/>
<br/>
<?= $order->get_formatted_billing_full_name() ?>様<br/>
<br/>
私どもオリジナルスマホケースラボをお選びいただき誠にありがとうございます。<br>
こちらのメールはご注文内容の自動確認メールとなります。<br>
ご注文の商品が到着するまでは、以下の様な流れになります。<br>
<br>
◆ご注文完了<br>
<br>
デザイン等不備がない場合制作の手続きを開始いたします。
通常、入金確認後、製作開始から4営業日目（土・日、祝日を除く）に発送します。<br>
<br>
◆商品出荷<br>
<br>
ご注文商品の出荷が完了致しましたら、各配送業者のお問い合わせ番号とともに<br>
【発送完了のお知らせ】メールを送信しております。<br>
通常、本州四国は出荷後翌日（離島除く）、その他翌々日着です。<br>
<br>

■発送完了までの流れ<br>
<br>
1.ご注文完了【今ここ】 → 2.工場へ制作依頼 →3.制作→ 4.発送<br>
<br>
※ご注意：<br>
工場にて在庫切れやデザイン不備が発覚した場合、<wbr>ご連絡メールをお送りします。予めご了承くださいませ。
<br><br>
■配送について<br>
<?php
/**
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );
?>
<br>
通常、製作開始から4営業日目（土・日、祝日を除く）<wbr>に発送します。<br>
<br>
■ご注文内容<br>
<?php
/**
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/**
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );
?>

天災などの理由により商品の配送が不可能となった場合、<br>
ご注文の再製作及びキャンセルし返金対応させて頂く場合がございます。<br>
尚、なんらかの事情によりお届けが難しくなった場合には、当サイトよりご連絡さしあげます。<br>
<br>
また、製作工場側にてお客様のデザインに不備などを発見した場合や<br>
商品のメーカー在庫切れなどが起こった場合など、<br>
サポートデスクよりお客様にご連絡させていただくこともございます。<br>
お客様からご回答がない場合、製作を進められない可能性もありますので<br>
連絡があった場合にはご回答をよろしくお願いいたします。<br>
<br>
利用規約<br>
→ <a href="https://original-smaphocase.com/kiyaku" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ja&q=https://original-smaphocase.com/kiyaku&source=gmail&ust=1512718156537000&usg=AFQjCNHdkm4hCqpX1vJjUmnEvFKucJ_OiA">https://original-smaphocase.com/kiyaku</a><br>
納期につきまして<br>
→ <a href="https://original-smaphocase.com/info/faq/delivery" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ja&q=https://original-smaphocase.com/info/faq/delivery&source=gmail&ust=1512718156537000&usg=AFQjCNFjlcW-BY5OqPV9Vj80TerQ7h6kDQ">https://original-smaphocase.com/info/faq/delivery</a><br>

その他ご注文に関する不明点、疑問点などお問い合わせに関しましては<br>
以下のお問い合わせ窓口よりご連絡ください。<br>
<br>
お問い合わせの前にこちらもご確認ください。<br>
FAQ（よくある質問）<a href="https://original-smaphocase.com/info/faq" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ja&q=https://original-smaphocase.com/info/faq&source=gmail&ust=1512718156537000&usg=AFQjCNEgrM8CenvDuwIiJcrRiRpqb7giDg">https://original-<wbr>smaphocase.com/info/faq</a><br>
<br>
このEメールアドレスは、配信専用です。このメッセージに返信しないようお願いいたします。<br>
<br>
―スマホラボ―――――――――――――――――――――<br>
URL（PC）： <a href="https://original-smaphocase.com/" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ja&q=https://original-smaphocase.com/&source=gmail&ust=1512718156537000&usg=AFQjCNElerRzMliSynAJGx8NaShh9ZWWGA">https://original-smaphocase.com/</a><br>
お問い合わせ（PC）： <a href="https://original-smaphocase.com/contact" target="_blank" data-saferedirecturl="https://www.google.com/url?hl=ja&q=https://original-smaphocase.com/contact&source=gmail&ust=1512718156537000&usg=AFQjCNFbqYFlUSY-eJiwXq70tRsARZnYGA">https://original-smaphocase.com/contact</a><br>
電話（月～金9時から18時まで）： 0120-570-150<br>
（姉妹サイトTMIXサポートデスクで対応致します。）
運営会社 ： 株式会社spice life<br>
―――――――――――――――――――――――――<br>
</p>
<?php

/**
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
