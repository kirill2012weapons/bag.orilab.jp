<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * @class  WCP_Printty2_Item_Color
 */
class WCP_Printty2_Item_Color extends WCP_Printty2_Master_Base {
	public function __construct($id, $product_id, $title, $hex ){
		$this->id = $id;
		$this->product_id = $product_id;
		$this->title = $title;
		$this->hex = $hex;
	}

	public function get_item_sides(){
		return WCP_Printty2_Item_Side::find_by_color_id($this->get_id());
	}

	static public function get_master_file_path(){
		return '/data/printty_item_colors.csv';
	}

	static public function find_by_product_id_and_title ($product_id, $title) {
		$file = static::get_master_file();
		foreach ($file as $line) {
			if(!is_null($line[0]) && strcmp($line[1], $product_id) == 0 && strcmp($line[2], $title) == 0 ){
				return new WCP_Printty2_Item_Color($line[0], $line[1], $line[2], $line[3]);
			}
		}
		return null;
	}

	static public function find_by_product_id($product_id) {
		$file = static::get_master_file();
		foreach ($file as $line) {
			if(!is_null($line[0]) && strcmp($line[1], $product_id) == 0){
				$records[] = new WCP_Printty2_Item_Color($line[0], $line[1], $line[2], $line[3]);
			}
		}

		return $records;
	}
}
