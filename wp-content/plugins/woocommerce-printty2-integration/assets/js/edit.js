jQuery( function ( $ ) {

  var status_order = [
    'all',
    'wc-on-hold',
    'wc-pending'
    'wc-processing',
    'wc-completed',
    'wc-processing-p2',
    'wc-sended-p2',
    'wc-cancelled',
    'wc-failed-p2',
    'wc-refunded',
    'trash',
  ];

  $(".wrap > ul.subsubsub").html (
    $(".wrap > ul.subsubsub > li").sort(function(a, b){
      var a_order = status_order.indexOf($(a).attr('class'))
      var b_order = status_order.indexOf($(b).attr('class'))

      return a_order - b_order;
    })
  )

  $(".wrap > ul.subsubsub li").each (function(){
    $(this).html($(this).html().replace(/ \|/, ''));
    $(this).addClass('add-split-deco');
  })

});
