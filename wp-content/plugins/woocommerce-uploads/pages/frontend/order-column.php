 <?php if ($has_uploads): ?>

    <?php do_action('wpf_umf_before_my_orders_uploads', $order, $uploads); ?>

        <?php if ($uploads_approved): ?>
            <span class="dashicons dashicons-yes"></span> <?php _e('Approved', $this->plugin_id); ?>
        <?php else: ?>
            <a href="<?php echo $order->get_view_order_url(); ?>" class="button"><?php _e( 'Upload', $this->plugin_id ); ?></a>
        <?php endif; ?>

    <?php do_action('wpf_umf_after_my_orders_uploads', $order, $uploads); ?>

 <?php else: ?>
  -
 <?php endif; ?>