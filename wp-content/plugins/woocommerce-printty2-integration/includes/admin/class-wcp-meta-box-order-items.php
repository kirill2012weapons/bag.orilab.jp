<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * 管理画面の注文商品のMetaboxです。
 */
class WCP_Meta_Box_Order_Items {

	// todo: replace get_option



	public function __construct(){
		add_filter( 'woocommerce_hidden_order_itemmeta', array( $this, 'filter_woocommerce_hidden_order_itemmeta' ));
		add_action( 'woocommerce_before_order_itemmeta', array( $this, 'action_woocommerce_before_order_itemmeta' ), 30, 3);

		add_action('admin_print_scripts-post-new.php', array( $this, 'action_admin_print_scripts'));
		add_action('admin_print_scripts-post.php', array( $this, 'action_admin_print_scripts'));
		add_action('admin_print_scripts-edit.php', array( $this, 'action_admin_print_scripts_edit'));
		add_action('admin_print_styles-post-new.php', array( $this, 'action_admin_print_styles'));
		add_action('admin_print_styles-edit.php', array( $this, 'action_admin_print_styles'));
		add_action('admin_print_styles-post.php', array( $this, 'action_admin_print_styles' ));
	}

	public function action_admin_print_scripts(){
		global $post_type;
		$prefix = "woocommerce_printty2_integration";
		if( 'shop_order' == $post_type ){
			wp_enqueue_script('jquery');
			wp_enqueue_script( "{$prefix}-select-media-js", plugins_url('woocommerce-printty2-integration/assets/js/select_media.js') );
		}
	}

	public function action_admin_print_scripts_edit(){
		global $post_type;
		$prefix = "woocommerce_printty2_integration";
		if( 'shop_order' == $post_type ){
			wp_enqueue_script('jquery');
			wp_enqueue_script( "{$prefix}-edit-js", plugins_url('woocommerce-printty2-integration/assets/js/edit.js') );
		}
	}

	public function action_admin_print_styles(){
		global $post_type;
		$prefix = "woocommerce_printty2_integration";
		if( 'shop_order' == $post_type ){
			wp_enqueue_style( "{$prefix}-main-js", plugins_url('woocommerce-printty2-integration/assets/style/main.css') );
		}
	}

	public function action_woocommerce_admin_order_items_after_line_items($item_id, $item, $order){
		wcp2i()->log("call");
	}


	public function filter_woocommerce_hidden_order_itemmeta($val){
		return array_merge($val, WCP_Order_Item_Product::get_data());
	}

	public function action_woocommerce_before_order_itemmeta($item_id, $item, $product){
		if( strcmp(get_class($item), 'WC_Order_Item_Product') != 0){
			return true;
		}

		$order = $item->get_order();
		$item_product = new WCP_Order_Item_Product($item);

?>
<div class="view">
<table cellspacing="0" class="display_meta">
	<tr>
		<th style="width: 50%;"><?php _e( 'Printty2 Image Url', 'woocommerce-printty2-integration' )?></th>
		<td>
			<label class="printty2-file-select">
				<div style="text-decoration: underline; color: blue; font-size: small;">Select File</div>
				<img src="<?php echo $item_product->get_image_url(true)?>" style="<?php echo $item_product->exists_image() ? "" : "display:none;" ?>height:100px;border: 1px solid #a5a5a5;margin: 5px;" />
				<input type="file" data-order-id="<?php echo $order->get_id() ?>" data-item-id="<?php echo $item_id?>" data-item-field="image" id="_order_item_printty2_image" name="order_item_printty2_image" value="" style="display:none;" accept="image/*"/>
			</label>
		</td>
	</tr>
	<tr>
		<th><?php _e( 'Printty2 Preview Url', 'woocommerce-printty2-integration' )?></th>
		<td>
			<label class="printty2-file-select">
				<div style="text-decoration: underline; color: blue; font-size: small;">Select File</div>
				<img src="<?php echo $item_product->get_preview_image_url(true)?>" style="<?php echo $item_product->exists_preview_image() ? "" : "display:none;" ?>height:100px;border: 1px solid #a5a5a5;margin: 5px;" />
				<input type="file" data-order-id="<?php echo $order->get_id() ?>" data-item-id="<?php echo $item_id?>" data-item-field="preview" id="order_item_printty2_preview" name="order_item_printty2_preview" value="" style="display:none;" accept="image/*"/>
			</label>
		</td>
	</tr>
</table>

</div>
<?php
	}

	public static function save() {
		$order = wc_get_order( $_POST['order_id'] );

		if ( sizeof( $order->get_items() ) > 0 ) {
			foreach ( $order->get_items() as $item ) {
				if ( $item->get_id() == $_POST['item_id'] ){
					$product = new WCP_Order_Item_Product($item);

					if ( isset( $_FILES['_printty2_image'] ) ) {
						$product->set_image($_FILES['_printty2_image']);
					}

					if ( isset( $_FILES['_printty2_preview_image'] ) ) {
						$product->set_preview_image($_FILES['_printty2_preview_image']);
					}
				}
			}
		}
	}

}
new WCP_Meta_Box_Order_Items();
