<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 *
 * @class WCP_Admin_Notice
 */
class WCP_Admin_Notice {
	function __construct(){
		add_action( 'admin_notices', array($this, 'show_admin_notices') );
	}

	function show_admin_notices(){
		$html  = '<div class="updated">';
		$html .= '  <p>テストメッセージ</p>';
		$html .= '</div>';
		return $html;
	}

}
new WCP_Admin_Notice();
